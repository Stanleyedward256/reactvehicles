﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using vehicle.models;

namespace vehicle_api.Context
{
    public class VehicleContext : DbContext
    {
        public DbSet<Journey> Journeys { get; set; }

        public DbSet<Vehicle> Vehicles { get; set; }

        public VehicleContext(DbContextOptions<VehicleContext> options) : base (options)
        {

        }
    }
}
