﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace vehicle.models
{
    /// <summary>
    /// Represents a single vehicle that we are interested in
    /// </summary>
    public class Vehicle
    {
        /// <summary>
        /// Database Id for this vehicle
        /// </summary>
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        /// <summary>
        /// The vehicle's registration number
        /// </summary>
        public string Registration { get; set; }

        /// <summary>
        /// The date that the MOT is due
        /// </summary>
        public DateTime MotDue { get; set; }

        /// <summary>
        /// The date that the next Service is due
        /// </summary>
        public DateTime ServiceDue { get; set; }

        /// <summary>
        /// A description of the vehicle
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// Journeys associated with this vehicle (one to many relationship)
        /// </summary>
        public ICollection<Journey> Journeys { get; set; }
    }
}
