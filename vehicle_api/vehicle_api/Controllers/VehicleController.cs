﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using vehicle.models;
using vehicle_api.Context;
using Microsoft.EntityFrameworkCore;

namespace vehicle_api.Controllers
{
    /// <summary>
    /// Our Vehicle controller.
    /// ActionRoutes on this use the standard VERBS 
    /// </summary>
    [ApiController]
    [Route("[controller]")]
    public class VehicleController : ControllerBase
    {
        private readonly VehicleContext _context;
        public VehicleController(VehicleContext context)
        {
            _context = context;
        }


        [HttpGet]
        [Route("Create")]
        public Vehicle Create()
        {
            return new Vehicle();
        }

        [HttpGet]
        [Route("{id}")]
        public Vehicle Read(int id)
        {
            var vehicle = _context.Vehicles.Where(v => v.Id == id).FirstOrDefault();
            return vehicle;
        }

        [HttpPost]
        [Route("")]

        public bool Update([FromBody]Vehicle model = null)
        {
            if (model.Id == 0)
            {
                _context.Add<Vehicle>(model);
            }
            else
            {
                _context.Update<Vehicle>(model);
            }
            _context.SaveChanges();

            return true;
        }

        [HttpDelete]
        [Route("{id}")]
        public bool Delete(int id)
        {
            var vehicle = _context.Vehicles.Where(v => v.Id == id).FirstOrDefault();
            if(vehicle == default)
            {
                return false;
            }
            else
            {
                _context.Remove<Vehicle>(vehicle);
                _context.SaveChanges();
                return true;
            }
            
        }

        [HttpGet]
        [Route("")]
        public IEnumerable<Vehicle> List()
        {
            return _context.Vehicles;
        }

    }
}
