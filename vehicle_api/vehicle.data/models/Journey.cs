﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace vehicle.data.models
{
    public class Journey
    {
        /// <summary>
        /// Database Id for this journey
        /// </summary>
        [Key]
        public int Id { get; set; }

        /// <summary>
        /// The vehicle this journey is for
        /// </summary>
        public Vehicle Vehicle { get; set; }

        /// <summary>
        /// The start time of the journey
        /// </summary>
        public DateTime Start { get; set; }

        /// <summary>
        /// The finish time of the journey
        /// </summary>
        public DateTime Finish { get; set; }

        /// <summary>
        /// How many miles the journey was
        /// </summary>
        public int MilesTravelled { get; set; }
    }
}
