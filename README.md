# React Demo application

This project was created to have a play with react.

It consists of two projects:

## vehicle_api

A .NET Core API, which uses Entity Framework to create and load a SqlServer database (I've you are building for the first time then don't forget to run 'Update-Database' in the Package Manager console).

There's not much to it, and if you want to actually see some data then you'll want to add some vehicles and set a correct connection string for your machine.

## vehicle_ui

This is the React frontend aspect of the project.

It can be started in the normal way 'npm start'

It was initialised using the npm react-template, uses the React-Bulma components, and uses Axios for calling the API.


