import React from 'react';
import { Card } from 'react-bulma-components';
import { Content } from 'react-bulma-components';
import { Heading } from 'react-bulma-components';
import 'react-bulma-components/dist/react-bulma-components.min.css';
/**
 The VehicleDetails component takes a prop of 'vehicle' and renders details of that vehicle on a card.
 */
export default class VehicleDetails extends React.Component
{
  render() {
    let vehicle = this.props.vehicle;
    // The rudiments of error handling...
    if(vehicle === undefined || vehicle === null)
    {
      return (
        <div>
          <Card>
            <Card.Content>
              <Heading size={4}>Oh dear...</Heading>
              <Heading subtitle size={6}>Somehow no vehicle information was found, did you set the 'vehicle' prop correctly?</Heading>
            </Card.Content>
          </Card>
          <br/>
        </div>
      );
    }
    else
    {
      return (
        <div>
          <Card>
            <Card.Content>
              <Heading size={4}>{vehicle.registration}</Heading>
              <Heading subtitle size={6}>{vehicle.description}</Heading>
              <Content>
                More information to follow...
              </Content>
            </Card.Content>
          </Card>
          <br/>
        </div>
      );
    }
  }
}