import React from 'react';
import axios from 'axios';
import VehicleDetails from "./VehicleDetails.jsx"
import 'react-bulma-components/dist/react-bulma-components.min.css';

/**
 A Component that on load requests all the vehicles from our api, then renders them using the VehicleDetails component.
 */
export default class VehicleList extends React.Component
{
  state = {
    vehicles: []
  }

  componentDidMount() {
    axios.get(`http://localhost:50106/vehicle`)
      .then(res => {
        const vehicles = res.data;
        this.setState({ vehicles });
      })
  }

  render() {
    let vehicles = this.state.vehicles.map((item) => <VehicleDetails vehicle={item}></VehicleDetails>);
    return <div class="is-fullwidth">{vehicles}</div>
  }
}