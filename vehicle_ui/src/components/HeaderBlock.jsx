import React from 'react';
import { Navbar } from 'react-bulma-components';
import 'react-bulma-components/dist/react-bulma-components.min.css';

/**
 * Represent the Header at the top of our pages.
 * This consists of our logo and a title.
 */
export default class HeaderBlock extends React.Component
{
  render() {
    return (
<Navbar>
        <Navbar.Brand>
          <Navbar.Item>
            <img src="logo.png" alt="Vehicle manager list"/>
          </Navbar.Item>
          <Navbar.Item><h1>Vehicle manager - A demonstration application using React</h1></Navbar.Item>
          
        </Navbar.Brand>
     </Navbar>
          )
  }
}