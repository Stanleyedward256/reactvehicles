import './App.css';
import 'react-bulma-components/dist/react-bulma-components.min.css';
import { Container } from "react-bulma-components";
import VehicleList from "./components/VehicleList.jsx"
import HeaderBlock from "./components/HeaderBlock.jsx"
import { Hero } from 'react-bulma-components';
import { Heading } from 'react-bulma-components';
import { Section } from 'react-bulma-components';
import 'react-bulma-components/dist/react-bulma-components.min.css';

/**
 It's got to start somewhere, so it starts here :)
 */
function App() {
    return (
      <div className="App">
        <Container>
          <HeaderBlock></HeaderBlock>
          <Section>
            <Hero color="info" >
              <Hero.Body>
                <Container>
                  <Heading>
                  What's all this about then?
                  </Heading>
                  <Heading subtitle size={6}>
                    Good question! What you are looking at is a demo app I wrote to get a feel for React.<br/><br/> It consists of three components;
                    The HeaderBlock containing the 'nav' above, a VehicleList component which uses axios to make a call for data from an api, and finally a VehicleDetails component that is used to render details about a single vehicle as a card.
                    <br/><br/>
                    Because I quite like the css framework I've imported the React-Bulma package and used those components for most of the layouts.
                    <br/><br/>
                    To a seasoned React developer this is probably fairly crude, and I am sure there are plenty of things that could be improved, but considering I'd never used React 4 hours ago I would call this a win! :)
                    <br/><br/>
                    The original plan was to add additional functionality to allow you to add and edit new vehicles, and the underlying api is setup to cater for this, but I didn't get that far.
                  </Heading>
                </Container>
              </Hero.Body>
            </Hero>
          </Section>
          <Heading>Vehicles in the system</Heading>
          <VehicleList></VehicleList>
        </Container>
      </div>
    );
  }

export default App;
